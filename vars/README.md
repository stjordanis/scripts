readonly variables, meant for usage by all scripts

## Files

### common
* Variables common to all devices, generic.

### devices
* List of supported devices

### $device
* Device-specific variables
